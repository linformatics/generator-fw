# generator-fw
> App/Addon Generator for the FW App System

[![Shippable](https://img.shields.io/shippable/5769d3002a8192902e248599.svg?maxAge=2592000)](https://app.shippable.com/projects/5769d3002a8192902e248599)

## Installation

First, install [Yeoman](http://yeoman.io) and generator-fw using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-fw
```

Then generate your new project:

```bash
yo fw
```

Running that will give you a list of generators to choose from. There are currently two:

 * Group Control Kit (gckit) - Generates an FW App based on the Group Control permissions structure
 * PHP Kit (phpkit) - Generates an FW Addon

You can optionally run one of those generators specifically:

```bash
yo fw:gckit # Runs the gckit generator
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Benner Library Informatics](http://library.olivet.edu)
