'use strict';
var path = require('path');
// var assert = require('yeoman-assert');
var helpers = require('yeoman-test');
// var fs = require('fs');
var expect = require('chai').expect;

describe('Integration: Main Generator', function () {
    describe('errors correctly', function () {
        var error;

        before(function () {
            return helpers.run(path.join(__dirname, '../../generators/app'))
                .inTmpDir(function () {})
                .catch(function (err) {
                    error = err;
                });
        });

        it('inside an invalid directory', function () {
            expect(error).to.not.be.undefined;
            expect(error.message).to.match(/You are not running Yeoman inside of a valid FW App System/);
        });
    });

    // before(function () {
    //     return helpers.run(path.join(__dirname, '../generators/app'))
    //         .withPrompts({someAnswer: true})
    //         .toPromise();
    // });
    //
    // it('creates files', function () {
    //     assert.file([
    //         'dummyfile.txt'
    //     ]);
    // });
});
