'use strict';

var validator = require('../../generators/utils/validator');
var expect = require('chai').expect;

describe('Unit: Validator', function () {
    describe('notEmpty', function () {
        it('returns true when not empty', function () {
            var result = validator.notEmpty('test string');

            expect(result).to.be.true;
        });

        it('returns an error message when empty', function () {
            var result = validator.notEmpty('');

            expect(result).to.match(/^You must supply a value/);
        });
    });

    describe('hexColorCode', function () {
        it('returns true if passed a valid hex code', function () {
            var firstCheck = validator.hexColorCode('#333');
            expect(firstCheck).to.be.true;

            var secondCheck = validator.hexColorCode('AFC365');
            expect(secondCheck).to.be.true;
        });

        it('returns error message when passed an invalid hex code', function () {
            var firstCheck = validator.hexColorCode('not a hex code');
            expect(firstCheck).to.not.be.true;
            expect(firstCheck).to.match(/^You must specify a valid hexadecimal/);

            var secondCheck = validator.hexColorCode('1234');
            expect(secondCheck).to.not.be.true;
            expect(secondCheck).to.match(/^You must specify a valid hexadecimal/);

            var thirdCheck = validator.hexColorCode('GASBAG');
            expect(thirdCheck).to.not.be.true;
            expect(thirdCheck).to.match(/^You must specify a valid hexadecimal/);
        });
    });

    describe('semver', function () {
        it('returns true if a valid semantic version', function () {
            var result = validator.semver('1.0.0');

            expect(result).to.be.true;
        });

        it('returns error message if not a valid semantic version', function () {
            var result = validator.semver('not a semantic version');

            expect(result).to.match(/You must supply a valid semantic/);
        });
    });
});
