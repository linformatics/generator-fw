# generator-fw CHANGELOG

## 0.3.2
 * [BUGFIX] outputDirectory and url handling isn't handled through the config anymore. That didn't make sense and was confusing initially, so it was fixed.
 * [CLEANUP] app version is no longer handled through the config. It pulls from the app's version if available, or it has a default for new apps.
 * More gckit blueprint updates

## 0.3.1
 * [BUGFIX] Running the generator in an app directory doesn't fail now
 * [BUGFIX] Fix gckit generator config props handling
 * gckit blueprint updates

## 0.3.0
 * [BUGFIX] Fix .npmignore replacing .gitignore everywhere
 * [BUGFIX] Various blueprint template updates
 * [FEATURE] Add app/addon to FW Api registry on create
 * [FEATURE] Initialize git repository on create
 * [CLEANUP] Various cleanup of generators

## 0.2.1
 * [BUGFIX] remove files array from package.json

## 0.2.0
 * [CLEANUP] Convert blueprints from submodules to regular included files
 * [FEATURE] Add phpkit generator

## 0.1.0
 * Setup basic app
 * Initial working version of gckit generator
 * Testing of various utilities
