# <%= properName %>

> <%= description %>

This README outlines the details of collaborating on this FW App.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [PHP](http://php.net/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)

You will also need a valid FW Api installation to build this addon in. See the [FW App System docs](http://linformatics.bitbucket.org/docs/main/) for more information.

## Installation

* `cd /path/to/fw-api/apps/`
* `git clone <repository-url>` this repository
* change into the new directory
* `yarn install`
* `gulp init`

Edit the `config.json` file and add two entries:

* `outputDirectory` - The directory you want your app to be output to (normally goes inside your local document root)
* `url` - If your app is not at the root of your local domain (e.g. mylocalsite.com/apps/my-app/), then you need to set this to whatever is between the domain and your app's folder (in the previous example: '/apps/my-app/')

*Note: outputDirectory must also contain the url at the end if you have defined a custom url*

## Running / Development

* `gulp watch`
* Visit your application at the url you specified.

### Running Tests

* `gulp validate` - Runs all of the tests
* `gulp lint` - Lints all the files
* `gulp test-unit` - Runs the server side unit tests using [PHPUnit](http://phpunit.de)
* `gulp test-ember` - Runs the client-side Ember tests.

### Building

* `gulp dev` - Builds your app in development mode
* `gulp prod` - Builds your app in production mode

### Deploying

`gulp release` generates a zip that you can then upload to the release repository.

## Further Reading / Useful Links

* [FW Docs](http://linformatics.bitbucket.org/docs/main/)
* [ember.js](http://emberjs.com)
