'use strict';

/*
 * FW App Kit Gulpfile v2.0
 *
 * Contains tasks necessary to setup and build the application.
 *
 * Sections:
 *
 * 1. Setup/Help Tasks
 * 2. Build/Copy Tasks
 * 3. Clean Tasks
 * 4. Testing/Validation Tasks
 * 5. Release Compilation Tasks
 * 6. Miscellaneous/Temporary Tasks
 *
 */
 const fs = require('fs-extra');
 const fw = require('./fw.json');
 const del = require('del');
 const path = require('path');
 const colors = require('ansi-colors');
 const assign = require('lodash.assign');

 const gulp = require('gulp');
 const help = require('gulp-help-four');
 help(gulp, {
     aliases: ['h'],
     hideEmpty: true,
     hideDepsMessage: true
 });

 let configDefaults = {
     outputDirectory: './public/',
     url: '/'
 };
 let appPath = path.resolve('./client');
 let clientShell = {cwd: appPath};
 let npm = (process.platform === 'win32') ? 'npm.cmd' : 'npm';
 let config;
 let apiRoot;

 try {
     config = assign({}, configDefaults, fs.readJsonSync('./config.json'));
 } catch (e) {
     config = configDefaults;
 }

 apiRoot = process.env.API_ROOT || (config.apiRoot ? path.resolve(config.apiRoot) : '../../');

 function shell(cmd, opts) {
     opts = opts || {};
     var silent = opts.silent || false;
     return function (cb) {
         var parts = cmd.split(' ');

         delete opts.silent;
         opts.stdio = silent ? 'ignore' : 'inherit';

         var cp = require('child_process').spawn(parts[0], parts.slice(1), opts);

         cp.on('error', cb);
         cp.on('exit', function (code) {
             if (code && code !== 0) {
                 cb(new Error('Command exited with code: ' + code));
                 return;
             }
             cb();
         });
     };
 }

 /*
  * 1. Setup/Help Tasks
  */

 gulp.task('init', 'Installs ember-cli npm dependencies', shell(`${npm} run setup`, clientShell));

 /*
  * 2. Build/Copy Tasks
  */
 gulp.task('dev', 'Builds the Ember app in the development environment', ['dist:clean'], shell(`${npm} run build`, clientShell));
 gulp.task('prod', 'Builds the Ember app in the production environment', ['dist:clean'], shell(`${npm} run build -- --environment=production`, clientShell));
 gulp.task('watch', 'Runs the app in watch mode', ['dist:clean'], shell(`${npm} run build -- --watch`, clientShell));
 gulp.task('build:release', shell(`${npm} run build -- --environment=production`, assign({}, clientShell, {env: assign({}, process.env, {RELEASE_BUILD: true})})));

 /*
  * 3. Clean Tasks
  */

 gulp.task('clean', 'Removes all built files and installed dependencies', ['dist:clean'], function () {
     return del(['client/dist/**', 'client/tmp/**']).then(function () {
         return del(['client/node_modules/**', 'node_modules/**']);
     });
 });

 gulp.task('clean:release', function () {
     return del('./public');
 });

 gulp.task('dist:clean', function () {
     if (config.outputDirectory === './' || config.outputDirectory === '/') {
         throw new Error(colors.red('cannot output to root or home directory'));
     }

     return del(config.outputDirectory, {force: true});
 });

 /*
  * 4. Testing/Validation Tasks
  */

 // 4.1 Linter Tasks
 gulp.task('phplint', function (cb) {
     require('phplint').lint(['./server/src/**/*.php'], {limit: 10}, cb);
 });

 gulp.task('phpcs', function () {
     var phpcs = require('gulp-phpcs');

     return gulp.src('./server/src/**/*.php')
         .pipe(phpcs({
             bin: apiRoot + '/vendor/bin/phpcs',
             standard: './standards/fw_phpcs/',
             colors: true
         }))
         .pipe(phpcs.reporter('log'))
         .pipe(phpcs.reporter('fail'));
 });

 gulp.task('lint', 'Lints all files and checks all files for code style errors', gulp.series('phplint', 'phpcs'));

 // 4.2 Testing Tasks
 gulp.task('test-ember', shell(`${npm} test`, clientShell));

 gulp.task('test-unit', shell(apiRoot + '/vendor/bin/phpunit --tap --colors=always', {
     cwd: process.cwd(),
     env: assign({}, process.env, {
         VENDOR_PATH: apiRoot + '/vendor/autoload.php'
     })
 }));

 gulp.task('validate', gulp.series('lint', 'test-unit', 'test-ember'));

 /*
  * 5. Release Compilation Tasks
  */
 gulp.task('config:release', function (done) {
     // Don't clobber existing config.json if present
     if (fs.existsSync('config.json')) {
         fs.renameSync('config.json', 'config-dev.json');
     }

     fs.writeJsonSync('config.json', configDefaults, {spaces: 2});
     done();
 });

 gulp.task('cleanup', function (done) {
     fs.removeSync('config.json');

     if (fs.existsSync('config-dev.json')) {
         fs.renameSync('config-dev.json', 'config.json');
     }
     done();
 });

 gulp.task('zip', function () {
     const zip = require('gulp-zip');

     return gulp.src([
         '!./server/config/local.php',
         '!./server/.*',
         '!./server/tests/**',
         '!./server/tests',
         './server/**/**',
         './public/**/**',
         './content/**/**',
         'fw.json',
         'install.php',
         'update.php'
     ], {base: './', allowEmpty: true})
         .pipe(zip(`${fw.version}.zip`))
         .pipe(gulp.dest('./'));
 });

 gulp.task('release', 'Compiles and zips the code for release', function (cb) {
     var sequence = require('gulp4-run-sequence');
     sequence('clean:release', 'config:release', 'build:release', 'zip', 'cleanup', cb);
 });

 /*
  * 6. Miscellaneous/Temporary Tasks
  */
