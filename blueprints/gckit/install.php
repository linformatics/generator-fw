<?php

return function (bool $update = false) {
    if (!$update) {
        $configType = $this->io->confirm('Do you want to set up config values for all apps?', true) ? 'global' : 'app';

        $this->prompt($configType, 'database', [
            'driver' => [
                'prompt' => 'Enter the database driver',
                'default' => 'mysql',
                'required' => true
            ],
            'host' => [
                'prompt' => 'Enter the database host',
                'default' => '127.0.0.1'
            ],
            'port' => [
                'prompt' => 'Enter the database port',
                'default' => '3306',
                'type' => 'int'
            ],
            'database' => [
                'prompt' => 'Enter the database name',
                'required' => true
            ],
            'username' => [
                'prompt' => 'Enter the database username'
            ],
            'password' => [
                'prompt' => 'Enter the database password',
                'hidden' => true
            ]
        ]);
    }

    $this->process('fw database migrate', 'Running database migrations');
};
