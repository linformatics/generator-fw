/* eslint-disable */

module.exports = {
    name: 'asset-delivery',
    postBuild: function (results) {
        var fs = this.project.require('fs-extra'),
            path = require('path'),
            walkSync = this.project.require('walk-sync'),
            root = this.project.root.replace(/\/?$/, '/'),
            config = fs.readJsonSync(path.join(root, '../config.json')),
            inDir = results.directory,
            assets = walkSync(inDir),
            apiRoot, outDir;

        if (!config.outputDirectory) return;

        outDir = (config.outputDirectory.charAt(0) !== '/') ?
            path.join(root, '..', config.outputDirectory) :
            config.outputDirectory;

        if (process.env.API_ROOT) apiRoot = process.env.API_ROOT;
        else if (config.apiRoot) {
            if (config.apiRoot.charAt(0) !== '/') {
                apiRoot = path.resolve(root, '..', config.apiRoot);
            } else {
                apiRoot = config.apiRoot;
            }
        } else apiRoot = path.resolve(root, '../../..');

        if (outDir.slice(-1) !== '/') {outDir += '/';}

        fs.ensureDirSync(outDir);

        assets.forEach(function (relativePath) {
            if (relativePath.slice(-1) === '/' || relativePath.match(/test/)) return;

            var inFile = path.join(inDir, relativePath),
                outFile = path.join(outDir, relativePath),
                contents;

            fs.ensureFileSync(outFile);

            if (relativePath.match(/api\/v[0-9\.]+\.php$/) && apiRoot && !process.env.RELEASE_BUILD) {
                contents = fs.readFileSync(inFile, 'utf8');

                contents = contents.replace(/\{\{api\}\}/, `${apiRoot.replace('/\\|\/$/', '')}/api.php`);

                fs.writeFileSync(outFile, contents, 'utf8');
            } else fs.copySync(inFile, outFile, {overwrite: true});
        });
    }
};
