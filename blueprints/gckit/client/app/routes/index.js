import AuthRoute from './auth';
import IndexMixin from '@bennerinformatics/ember-fw-gc/mixins/index-route';

export default AuthRoute.extend(IndexMixin, {
    title: 'Index',
    mainRoute: '' // This should be set to the page route you want to be loaded first
});
