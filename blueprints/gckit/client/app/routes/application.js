import {inject} from '@ember/service';
import Route from '@ember/routing/route';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import DepartmentTransitionMixin from '@bennerinformatics/ember-fw-gc/mixins/department-transition';

export default Route.extend(ApplicationRouteMixin, DepartmentTransitionMixin, {
    title: 'Application',

    config: inject(),

    sessionInvalidated() {
        this.send('reload');
    },

    actions: {
        reload() {
            window.location.replace(this.get('config.url'));
        }
    }
});
