import Controller from '@ember/controller';
import {inject} from '@ember/service';
import ApplicationControllerMixin from '@bennerinformatics/ember-fw-gc/mixins/application-controller';

export default Controller.extend(ApplicationControllerMixin, {
});
