import {computed} from '@ember/object';
import Router from '@ember/routing/router';
import {inject} from '@ember/service';
import authRouter from '@bennerinformatics/ember-fw-gc/router';
import envConfig from './config/environment';

const EmberRouter = Router.extend({
    location: envConfig.locationType,

    config: inject(),

    rootURL: computed(function () {
        return this.get('config.url') || envConfig.baseURL;
    })
});

EmberRouter.map(function () {
    // Add your app-specifc routes here

    // Don't remove this as it is needed to setup important routes
    authRouter(this);
});

export default EmberRouter;
