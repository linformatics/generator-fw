/* eslint-env node */
/* eslint-disable no-var */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var nodeSass = require('node-sass');

module.exports = function (defaults) {
    var app = new EmberApp(defaults, {
        fingerprint: {
            exclude: ['fonts/**', 'assets/images/**', 'assets/icons/**']
        },
        sassOptions: {
            implementation: nodeSass
        }
    });

    return app.toTree();
};
