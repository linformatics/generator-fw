module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  plugins: [
    'ember'
  ],
  extends: [
    'eslint:recommended',
    'plugin:ember/recommended',
    'plugin:ember-suave/recommended'
  ],
  env: {
    browser: true
  },
  rules: {
    indent: ['error', 4],
    'space-before-function-paren': ['error', {anonymous: 'ignore', named: 'never'}],
    'object-curly-spacing': ['error', 'never'],
    'array-bracket-spacing': ['error', 'never'],
    'ember/no-new-mixins': ['off'],
    'key-spacing': ['error', {mode: 'minimum'}],
    'ember/avoid-leaking-state-in-ember-objects': ['warn', [
        'classNames',
        'classNameBindings',
        'actions',
        'concatenatedProperties',
        'mergedProperties',
        'positionalParams',
        'attributeBindings',
        'queryParams',
        'attrs',

        'roles'
    ]]
  },
  overrides: [
    // node files
    {
      files: [
        'testem.js',
        'ember-cli-build.js',
        'config/**/*.js',
        'lib/*/index.js'
      ],
      parserOptions: {
        sourceType: 'script',
        ecmaVersion: 2015
      },
      env: {
        browser: false,
        node: true
      }
    }
  ]
};
