import env from '<%= shortcode %>/config/environment';

let apiRoot = env.APP.config.url + env.APP.config.api;

export default function () {
    this.namespace = apiRoot;
}
