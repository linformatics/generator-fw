<?php

if (getenv('FW_API_PATH')) {
    require getenv('FW_API_PATH');
} else {
    // Installer runs from app directory
    require getcwd() . '/../../api.php';
}

$adapter = FW::init('<%= shortcode %>', false, ['adapter'])->container('module.adapter');

return [
    'migration_base_class' => 'MigrationCore\GroupControlMigration',
    'paths' => [
        'migrations' => __DIR__ . '/migrations'
    ],
    'environments' => [
        'default_migration_table' => '<%= shortcode %>_migration_log',
        'default_environment' => 'main',
        'main' => [
            'name' => $adapter->getDbName(),
            'connection' => $adapter->getPdo()
        ]
    ]
];
