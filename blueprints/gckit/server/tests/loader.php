<?php

$loader = require(getenv('VENDOR_PATH'));

$loader->addPsr4('<%= namespace %>\\', realpath(__DIR__ . '/../src/'));
