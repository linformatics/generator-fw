<?php

namespace <%= namespace %>;

use FW\Router\Standard\AbstractRouteMapper;
use FW\Router\Standard\RoutingUtils;
/**
 * This class maps api requests (e.g. api/v1.php/users/find)
 * to specific classes (called 'controllers') and specific methods
 * in those classes (called 'actions')
 */
class Router extends AbstractRouteMapper {
    use RoutingUtils;

    /**
     * This function maps specific routes to controller actions
     * The method is passed a Slim application instance, which is
     * use to create specific routes and groups
     *
     * For further documentation on how to create routes,
     * consult the Slim Framework documentation
     * (http://docs.slimframework.com)
     *
     * @param  \Slim\Slim $app The Slim application
     */
    public function mapRoutes() {
    }
}
