<?php

namespace <%= namespace %>\Controller;

use GroupControl\Controller\AuthController as Base;

/**
 * This exists as a stub so that you can say
 * `class MyController extends AuthController`
 * in all of your controllers without having to
 * have the full GroupControl path each time.
 */
class AuthController extends Base {
}
