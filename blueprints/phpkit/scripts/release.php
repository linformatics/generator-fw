<?php

chdir(__DIR__ . '/..');

$composer = json_decode(file_get_contents('./fw.json'), true);
$zipFile = "./{$composer['version']}.zip";

$zip = new ZipArchive();

if ($zip->open($zipFile, ZipArchive::CREATE) !== true) {
    die('Zip file could not be created.');
}

$filesToAdd = [
    'composer.json',
    'composer.lock',
    'fw.json'
];

foreach ($filesToAdd as $file) {
    $zip->addFile($file);
}

$src = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator('src'),
    RecursiveIteratorIterator::SELF_FIRST
);

foreach ($src as $srcFile) {
    // Convert windows directory separators to posix separators
    $srcFile = str_replace('\\', '/', $srcFile);

    // skip . and .. folders
    if (in_array(substr($srcFile, strrpos($srcFile, '/') + 1), ['.', '..'])) continue;

    if (is_file($srcFile)) {
        $zip->addFile($srcFile);
    }
}

$zip->close();
