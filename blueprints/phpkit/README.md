# <%= properName %>

> <%= description %>

This README outlines the details of collaborating on this FW Addon.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com)
* [PHP](http://php.net)
* [Composer](https://getcomposer.org/)

You will also need a valid FW Api installation to build this addon in. See the [FW App System docs](http://linformatics.bitbucket.org/docs/main/) for more information.

## Installation

To install this addon, first cd into your FW Api installation's `addons` folder. (ex: `cd /path/to/fw-api/addons/`).

Then:

* `git clone <repository-url> addon-name` (clone this repository)
* change into the newly cloned directory
* `composer install`

To test this addon, you need to require it in an app's `fw.json` depdencies.

example: `fw-api/apps/some-application/fw.json`
```json
{
  "dependencies": {
    "addons": {
      "this-addon": "<addon-version>"
    }
  }
}
```

In order to test this addon while developing, you currently will need to symlink the folder into your app's addon folder. A better solution is currently in development.

## Running Tests

Tests go in the `tests` folder and are run with [PhpUnit](http://phpunit.de).

* Running all tests (linting and phpunit tests): `composer run-script test`
* Running linting: `composer run-script lint`
* Running just unit tests: `phpunit`

### Deploying

`composer run-script release` generates a zip that you can upload to the release repository so it can be installed elsewhere.