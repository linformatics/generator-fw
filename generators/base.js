'use strict';
const _ = require('lodash');
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const path = require('path');
const mkdirp = require('fs-extra').mkdirpSync;
const Promise = require('bluebird');

module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);

        this.config.path = path.join(this.destinationRoot(), '.fw-generator.json');
    }

    /**
     * Checks whether or not the current directory is a valid directory for
     * the FW Generator to run in.
     * @return {[type]} [description]
     */
    checkValidDirectory() {
        if (
            !this.fs.exists(this.destinationPath('.fw')) &&
            !this.config.get('alreadyGenerated') &&
            !this.fs.exists(this.destinationPath('fw.json'))
        ) {
            var message = chalk.red(
                'You are not running Yeoman inside of a valid FW App System api, app, or addon.\n' +
                'Please run this generator again within a valid directory.'
            );

            this.log.error(message);
            throw new Error(message);
        }
    }

    checkConfig() {
        var currentProps = this.config.get('props') || {};
        var propsRequired = this.requiredProperties || [];

        if (!propsRequired) {
            return true;
        }

        if (!currentProps) {
            return false;
        }

        return _.every(propsRequired, function (key) {
            return _.has(currentProps, key);
        });
    }

    generatePrompts(edit) {
        var diffKeys = (edit) ? this.requiredProperties :
            _.difference(this.requiredProperties || [], _.keys(this.config.get('props') || {}));

        var existingProps = this.config.get('props') || {};
        var setPrompts = this.prompts || {};
        var prompts = [];

        _.forEach(diffKeys, function (diffKey) {
            if (_.has(setPrompts, diffKey)) {
                var prompt = _.get(setPrompts, diffKey);

                if (edit && _.has(existingProps, diffKey)) {
                    prompt.default = _.get(existingProps, diffKey);
                }

                prompts.push(prompt);
            }
        });

        return prompts;
    }

    changeDestinationRoot(name, type) {
        var newRoot;
        var fwRoot;

        if (this.fs.exists(this.destinationPath('.fw'))) {
            // We are inside the fw-api main directory, switch to apps or addons dir
            fwRoot = this.destinationRoot();
            newRoot = path.join(this.destinationRoot(), (type === 'app') ? 'apps' : 'addons', name);
        } else if (this.fs.readJSON(this.destinationPath('fw.json'), {}).appId && type !== 'app') {
            // We are inside of an app folder, and we're generating an addon, so we need to go to the addons folder
            fwRoot = process.env.FW_API_PATH || path.resolve(path.join(this.destinationRoot(), '../../'));
            newRoot = path.join(fwRoot, 'addons', name);
        } else {
            fwRoot = process.env.FW_API_PATH || path.resolve(path.join(this.destinationRoot(), '../../'));
            return;
        }

        try {
            mkdirp(newRoot);
        } catch (error) {
            this.log.error(error);
            throw error;
        }

        this.destinationRoot(newRoot);
        this.config.path = path.join(newRoot, '.fw-generator.json');
    }

    setGenerated() {
        var self = this;

        return new Promise(function (resolve) {
            self.config.set('alreadyGenerated', true);
            self.fs.commit(resolve);
        });
    }
};
