'use strict';
var _ = require('lodash');
var validator = require('../utils/validator');
var setupGit = require('../utils/git');
var path = require('path');
var walkSync = require('walk-sync');
var BaseGenerator = require('../base');

// These exist so that if the required properties
// change later on, we can update this and the user will
// be prompted for whatever properties are missing.
var requiredProperties = [
    'name',
    'groupName',
    'description',
    'version',
    'author',
    'authorEmail',
    'license',
    'repository',
    'properName',
    'namespace'
];

// Somehow the second parmeter is being set causing trim to trim more than just whitespace
const trim = v => _.trim(v);
var prompts = {
    name: {
        type: 'input',
        name: 'name',
        message: 'Addon Name (example: \'My Addon\')',
        filter: _.kebabCase,
        validate: validator.notEmpty
    },
    groupName: {
        type: 'input',
        name: 'groupName',
        default: 'linformatics',
        message: 'Group Name',
        filter: _.kebabCase,
        validate: validator.notEmpty
    },
    description: {
        type: 'input',
        name: 'description',
        message: 'Addon Description',
        filter: trim
    },
    version: {
        type: 'input',
        name: 'version',
        default: '0.1.0',
        message: 'Addon Version',
        validate: validator.semver
    },
    author: {
        type: 'input',
        name: 'author',
        message: 'Addon Author',
        filter: trim,
        validate: validator.notEmpty
    },
    authorEmail: {
        type: 'input',
        name: 'authorEmail',
        message: 'Addon Author Email',
        filter: trim,
        validate: validator.chain([
            validator.notEmpty,
            validator.email
        ])
    },
    license: {
        type: 'input',
        name: 'license',
        message: 'Addon License',
        default: 'MIT',
        filter: trim,
        validate: validator.notEmpty
    },
    repository: {
        type: 'input',
        name: 'repository',
        message: 'Addon Repository',
        filter: trim,
        validate: validator.chain([
            validator.notEmpty,
            validator.url
        ])
    }
};

module.exports = class extends BaseGenerator {
    constructor(args, opts) {
        super(args, opts);

        // These exist so that if the required properties
        // change later on, we can update this and the user will
        // be prompted for whatever properties are missing.
        this.requiredProperties = requiredProperties;
        this.prompts = prompts;

        // This gives the user the ability to optionally edit the
        // configuration data for an app or addon, in case of typos,
        // name changes, etc.
        this.option('edit', {
            desc: 'Edit the configured addon metadata',
            alias: 'e'
        });

        // Change source root to one that contains the blueprints
        this.sourceRoot(path.join(__dirname, '../../blueprints/phpkit'));
    }

    initializing() {
        this.checkValidDirectory();
    }

    prompting() {
        if (this.checkConfig() && !this.options.edit) {
            this.props = this.config.get('props');
            return;
        }

        return this.prompt(this.generatePrompts(this.options.edit)).then(function (props) {
            this.props = props;

            // The name is used in a couple of different places, so we do
            // some massaging of the inputted name to ensure format is correct
            // where needed.

            // Proper Name is the name of the app with each word separated by spaces
            // and capitalized
            this.props.properName = _.startCase(props.name);

            // The namespace is essentially the proper name with spaces removed.
            this.props.namespace = _.replace(this.props.properName, /\s+/g, '');
        }.bind(this));
    }

    configuring() {
        this.changeDestinationRoot(this.props.name, 'addon');
        this.config.set('props', this.props);

        var done = this.async();

        this.fs.commit(done);
    }

    writing() {
        var templateFiles = walkSync(this.sourceRoot(), {directories: false});

        _.forEach(templateFiles, _.bind(function (templateFile) {
            var outputFile = templateFile;

            // So npm in their infinite wisdom decided to
            // automatically rename .gitignore files to .npmignore
            // when a package is published. This changes it back.
            if (templateFile.indexOf('.npmignore') !== -1) {
                outputFile = templateFile.replace(/.npmignore$/, '.gitignore');
            }

            this.fs.copyTpl(
                this.templatePath(templateFile),
                this.destinationPath(outputFile),
                this.props
            );
        }, this));
    }

    install() {
        var self = this;

        return Promise.all([
            setupGit(this, {submodule: 'https://bitbucket.org/linformatics/fw-standards standards/fw_phpcs'}).then(function () {
                return self.setGenerated();
            }),
            this.scheduleInstallTask('composer')
        ]);
    }

    end() {
        this.log('Addon generated successfully!');
    }
};
