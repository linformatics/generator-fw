'use strict';
var chalk = require('chalk');
var yosay = require('yosay');
var BaseGenerator = require('../base');

module.exports = class extends BaseGenerator {
    initializing() {
        this.checkValidDirectory();
    }

    prompting() {
        // Have Yeoman greet the user.
        this.log(yosay(
            'Welcome to the ' + chalk.green('FW App System') + ' generator!'
        ));

        return this.prompt([{
            type: 'list',
            name: 'command',
            message: 'Please specify a kit to generate',
            choices: [
                {
                    name: 'Group Control App Kit',
                    value: 'gckit'
                },
                // @TODO: re-add once fw-kit has been updated
                // {
                //     name: 'FW App Kit',
                //     value: 'fwkit'
                // },
                {
                    name: 'PHP Addon Kit',
                    value: 'phpkit'
                }
            ]
        }]).then(function (props) {
            this.composeWith('@bennerinformatics/fw:' + props.command, {options: this.options});
        }.bind(this));
    }
};
