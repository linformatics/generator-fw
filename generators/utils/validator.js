var _ = require('lodash');
var semver = require('semver');
var validator = require('validator');

module.exports = {
    notEmpty: function (value) {
        return !_.isEmpty(value) || 'You must supply a value';
    },

    hexColorCode: function (value) {
        var hexColorRegex = /^#?([0-9A-Fa-f]{3}|[0-9A-Fa-f]{6})$/;

        return hexColorRegex.test(value) || 'You must specify a valid hexadecimal color code.';
    },

    semver: function (value) {
        return semver.valid(value) ? true : 'You must supply a valid semantic version.';
    },

    url: function (value) {
        return validator.isURL(value, {
            /* eslint-disable camelcase */
            require_protocol: true,
            require_valid_protocol: false,
            require_tld: true
            /* eslint-enable camelcase */
        }) || 'You must supply a valid url.';
    },

    email: function (value) {
        return validator.isEmail(value) || 'You must supply a valid email.';
    },

    chain: function (validators) {
        return function (value) {
            var message;

            _.some(validators, function (validator) {
                var result = validator(value);

                if (!_.isBoolean(result)) {
                    message = result;
                    return true;
                }

                return !result;
            });

            return message || true;
        };
    }
};
