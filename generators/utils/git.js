var Promise = require('bluebird');
var chalk = require('chalk');
var exec = Promise.promisify(require('child_process').exec);

module.exports = function (generator, {submodule} = {}) {
    if (generator.config.get('alreadyGenerated')) {
        return Promise.resolve();
    }

    var execOpts = {
        env: {
            GIT_AUTHOR_NAME: 'Informatics Generator',
            GIT_AUTHOR_EMAIL: 'informatics@bennerlibrary.com',
            get GIT_COMMITTER_NAME() {
                return this.GIT_AUTHOR_NAME;
            },
            get GIT_COMMITTER_EMAIL() {
                return this.GIT_AUTHOR_EMAIL;
            }
        },
        cwd: generator.destinationRoot()
    };
    var remote = generator.props.repository;
    var log = generator.log;

    return exec('git --version').then(function () {
        return exec('git init', execOpts).then(function () {
            if (submodule) {
                return exec(`git submodule add ${submodule}`);
            }

            return Promise.resolve();
        }).then(function () {
            return exec('git add .', execOpts);
        }).then(function () {
            return exec('git commit -m "Initial commit from the FW Generator"', execOpts);
        }).then(function () {
            return exec('git remote add origin ' + remote);
        }).then(function () {
            log(chalk.green('Successfully initialized git.'));
        });
    }).catch(function () {
        log(chalk.red('Git not installed. You will have to work with Git manually.'));
    });
};
