'use strict';
var _ = require('lodash');
var validator = require('../utils/validator');
var setupGit = require('../utils/git');
var path = require('path');
var walkSync = require('walk-sync');
var BaseGenerator = require('../base');

// These exist so that if the required properties
// change later on, we can update this and the user will
// be prompted for whatever properties are missing.
var requiredProperties = [
    'name',
    'shortName',
    'shortcode',
    'description',
    'color',
    'author',
    'license',
    'repository',
    'properName',
    'namespace',
    'linkName'
];

// Somehow the second parmeter is being set causing trim to trim more than just whitespace
const trim = v => _.trim(v);
var prompts = {
    name: {
        type: 'input',
        name: 'name',
        message: 'App Name (example: \'My App\')',
        filter: _.kebabCase,
        validate: validator.notEmpty
    },
    shortName: {
        type: 'input',
        name: 'shortName',
        message: 'Short Name (Used for mobile. Leave blank if App Name is short enough.)',
        default: '',
        filter: trim
    },
    shortcode: {
        type: 'input',
        name: 'shortcode',
        message: 'App Shortcode (example: \'app\')',
        filter: _.flow([_.toLower, function (value) {
            return _.replace(value, /\s+/g, '');
        }]),
        validate: validator.notEmpty
    },
    description: {
        type: 'input',
        name: 'description',
        message: 'App Description',
        filter: trim
    },
    color: {
        type: 'input',
        name: 'color',
        message: 'App Base Color (as hexadecimal)',
        default: '7E3DB7',
        filter: function (value) {
            return _.trimStart(value, '#');
        },
        validate: validator.hexColorCode
    },
    author: {
        type: 'input',
        name: 'author',
        message: 'App Author',
        filter: trim,
        validate: validator.notEmpty
    },
    license: {
        type: 'input',
        name: 'license',
        message: 'App License',
        default: 'MIT',
        filter: trim,
        validate: validator.notEmpty
    },
    repository: {
        type: 'input',
        name: 'repository',
        message: 'App Repository',
        filter: trim,
        validate: validator.chain([
            validator.notEmpty,
            validator.url
        ])
    }
};

module.exports = class extends BaseGenerator {
    constructor(args, opts) {
        super(args, opts);

        // These exist so that if the required properties
        // change later on, we can update this and the user will
        // be prompted for whatever properties are missing.
        this.requiredProperties = requiredProperties;
        this.prompts = prompts;

        // This gives the user the ability to optionally edit the
        // configuration data for an app or addon, in case of typos,
        // name changes, etc.
        this.option('edit', {
            desc: 'Edit the configured application metadata',
            alias: 'e'
        });

        // Change source root to one that contains the blueprints
        this.sourceRoot(path.join(__dirname, '../../blueprints/gckit'));
    }

    initializing() {
        this.checkValidDirectory();
    }

    prompting() {
        if (this.checkConfig() && !this.options.edit) {
            this.props = this.config.get('props');
            return;
        }

        var prompts = this.generatePrompts(this.options.edit);
        var existingConfig = this.fs.readJSON(this.destinationPath('config.json'), {});

        if (!existingConfig.outputDirectory || !existingConfig.url) {
            prompts.outputDirectory = {
                type: 'input',
                name: 'outputDirectory',
                message: 'Local server document root',
                filter: path.resolve
            };
            prompts.url = {
                type: 'App Base Path',
                name: 'url',
                message: 'App Base Path (e.g. /apps/app-name/)',
                filter: function (value) {
                    if (!value) {
                        return '/';
                    }

                    return '/' + _.trim(value, '/') + '/';
                }
            };
        }

        return this.prompt(prompts).then(function (props) {
            this.props = props;

            // The name is used in a couple of different places, so we do
            // some massaging of the inputted name to ensure format is correct
            // where needed.

            // Proper Name is the name of the app with each word separated by spaces
            // and capitalized
            this.props.properName = _.startCase(props.name);

            // Short Name is only needed if the Proper Name is too long to display as a title on mobile
            if (!this.props.shortName) {
                this.props.shortName = this.props.properName;
            }

            // The namespace is essentially the proper name with spaces removed.
            this.props.namespace = _.replace(this.props.properName, /\s+/g, '');

            // Link name replaces all spaces with '+' so they are able to be used in links, for example how the icons key works.
            this.props.linkName = _.replace(this.props.properName, / /g, '+');
        }.bind(this));
    }

    configuring() {
        this.changeDestinationRoot(this.props.shortcode, 'app');
        this.config.set('props', _.omit(this.props, ['outputDirectory', 'url'])); // Set existing properties to the config

        if (this.props.url || this.props.outputDirectory) {
            // Write config.json file
            var configFile = path.join(this.destinationRoot(), 'config.json');
            this.fs.delete(configFile);
            this.fs.writeJSON(configFile, _.pick(this.props, ['outputDirectory', 'url']));
        }

        // Var done = this.async();
        //
        // // Force the config to write to disk so we don't get conflict errors.
        // this.fs.commit(done);
    }

    writing() {
        var templateFiles = walkSync(this.sourceRoot(), {directories: false});
        var context = _.omit(this.props, ['outputDirectory', 'url']);

        context.version = this.fs.readJSON(this.destinationPath('package.json'), {}).version || '0.0.1';

        _.forEach(templateFiles, _.bind(function (templateFile) {
            var outputFile = templateFile;

            // So npm in their infinite wisdom decided to
            // automatically rename .gitignore files to .npmignore
            // when a package is published.
            // Because of this, we have renamed the .gitignore files to _gitignore
            // This changes it back
            if (templateFile.indexOf('_gitignore') !== -1) {
                outputFile = templateFile.replace(/_gitignore$/, '.gitignore');
            }

            this.fs.copyTpl(
                this.templatePath(templateFile),
                this.destinationPath(outputFile),
                context
            );
        }, this));
    }

    install() {
        var self = this;

        return Promise.all([
            setupGit(this, {submodule: 'https://bitbucket.org/linformatics/fw-standards standards/fw_phpcs'}).then(function () {
                return self.setGenerated();
            }),
            this.installDependencies({bower: false, yarn: true, npm: false})
        ]);
    }

    end() {
        this.log('App generated successfully!');
    }
};
