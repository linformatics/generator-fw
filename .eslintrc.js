module.exports = {
    root: true,
    extends: 'xo',
    rules: {
        indent: ['error', 4, {SwitchCase: 1}]
    },
    env: {
        mocha: true
    }
}
